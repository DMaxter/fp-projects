# Aluno Daniel Maximiano Matos    Numero 89429

def e_artigo_def(letra):
    """caracter -> booleano
        Verifica se letra e um artigo definido"""
    return (len(letra) == 1) and \
            letra in "AO"

def e_vogal_palavra(letra):
    """caracter -> booleano
        Verifica se letra e uma vogal e uma palavra simultaneamente"""
    return (len(letra) == 1) and \
            (letra == "E" or e_artigo_def(letra))

def e_vogal(letra):
    """caracter -> booleano
        Verifica se letra e uma vogal"""
    return (len(letra) == 1) and \
            (letra in "IU" or e_vogal_palavra(letra))

def e_ditongo_palavra(silaba):
    """cadeia de caracteres -> booleano
        Verifica se silaba e um ditongo e uma palavra simultaneamente"""
    return (len(silaba) == 2) and \
            (silaba in ("AI", "AO", "EU", "OU"))

def e_ditongo(silaba):
    """cadeia de caracteres -> booleano
        Verifica se silaba e um ditongo"""
    return (len(silaba) == 2) and \
            (silaba in ("AE", "AU", "EI", "OE", "OI", "IU") or \
            e_ditongo_palavra(silaba))

def e_par_vogais(par):
    """cadeia de caracteres -> booleano
        Verifica se silaba e um par de vogais"""
    return (len(par) == 2) and \
            (par in ("IA", "IO")) or (e_ditongo(par))

def e_consoante_freq(letra):
    """caracter -> booleano
        Verifica se letra e uma consoante que aparece frequentemente"""
    return (len(letra) == 1) and \
            letra in "DLMNPRSTV"

def e_consoante_terminal(letra):
    """caracter -> booleano
        Verifica se letra e uma consoante que fica no fim de uma palavra"""
    return (len(letra) == 1) and \
            letra in "LMRSXZ"

def e_consoante_final(letra):
    """caracter -> booleano
        Verifica se letra e uma consoante que fica no fim de uma silaba"""
    return (len(letra) == 1) and \
            (letra in "NP" or e_consoante_terminal(letra))

def e_consoante(letra):
    """caracter -> booleano
        Verifica se letra e uma consoante"""
    return (len(letra) == 1) and \
            letra in "BCDFGHJLMNPQRSTVXZ"

def e_par_consoantes(par):
    """cadeia de caracteres -> booleano
        Verifica se letra e um par de consoantes"""
    return (len(par) == 2) and \
            par in ("BR", "CR", "FR", "GR", "PR", "TR", "VR", "BL", "CL", "FL", "GL", "PL")

def e_silaba_2(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba de 2 letras de acordo com a gramatica"""
    return (len(string) == 2) and \
            (e_par_vogais(string) or \
            (e_consoante(string[0]) and e_vogal(string[1])) or \
            (e_vogal(string[0]) and e_consoante_final(string[1])))

def e_silaba_3(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba de 3 letras de acordo com a gramatica"""
    return (len(string) == 3) and \
            ((string in ("QUA", "QUE", "QUI", "GUE", "GUI")) or \
            (e_vogal(string[0]) and string[1:] == "NS") or \
            (e_consoante(string[0]) and e_par_vogais(string[1:])) or \
            (e_consoante(string[0]) and e_vogal(string[1]) and e_consoante_final(string[2])) \
            or (e_par_vogais(string[:2]) and e_consoante_final(string[2])) or \
            (e_par_consoantes(string[:2]) and e_vogal(string[2])))

def e_silaba_4(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba de 4 letras de acordo com a gramatica"""
    return (len(string) == 4) and \
            ((e_par_vogais(string[:2]) and string[2:] == "NS") or \
            (e_consoante(string[0]) and e_vogal(string[1]) and string[2:] == "NS") or \
            (e_consoante(string[0]) and e_vogal(string[1]) and string[2:] == "IS") or \
            (e_par_consoantes(string[:2]) and e_par_vogais(string[2:])) or \
            (e_consoante(string[0]) and e_par_vogais(string[1:3]) and \
            e_consoante_final(string[3])))

def e_silaba_5(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba de 5 letras de acordo com a gramatica"""
    return (len(string) == 5) and \
            (e_par_consoantes(string[:2]) and e_vogal(string[2]) and string[3:] == "NS")

def e_silaba_final(string):
    """cadeia de caracteres -> booleano
        Verifica se string e a silaba final de uma palavra, de acordo com a gramatica"""
    return e_monossilabo_2(string) or \
            e_monossilabo_3(string) or \
            e_silaba_4(string) or \
            e_silaba_5(string)

def e_silaba(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba de uma palavra, de acordo com a gramatica"""
    verifica_arg(string, "e_silaba")

    return e_vogal(string) or \
            e_silaba_2(string) or \
            e_silaba_3(string) or \
            e_silaba_4(string) or \
            e_silaba_5(string)

def e_monossilabo_2(silaba):
    """cadeia de caracteres -> booleano
        Verifica se silaba e  uma silaba de 2 letras e uma palavra,"""

    return (len(silaba) == 2) and \
            (silaba in ("AR", "IR", "EM", "UM") or \
            (e_vogal_palavra(silaba[0]) and silaba[1] == "S") or \
            e_ditongo_palavra(silaba) or \
            (e_consoante_freq(silaba[0]) and e_vogal(silaba[1])))

def e_monossilabo_3(silaba):
    """cadeia de caracteres -> booleano
        Verifica se silaba e uma silaba de 3 letras e uma palavra, simultaneamente"""

    return (len(silaba) == 3) and \
            ((e_consoante(silaba[0]) and e_vogal(silaba[1]) and \
            e_consoante_terminal(silaba[2])) or \
            (e_consoante(silaba[0]) and e_ditongo(silaba[1:])) or \
            (e_par_vogais(silaba[:2]) and e_consoante_terminal(silaba[2])))

def e_monossilabo(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma silaba e uma palavra simultaneamente"""
    verifica_arg(string, "e_monossilabo")

    return e_vogal_palavra(string) or \
            e_monossilabo_2(string) or \
            e_monossilabo_3(string)

def verifica_arg(argumento, funcao):
    """cadeia de caracteres x cadeia de caracteres -> None
        Verifica se o argumento e uma string"""
    if(not isinstance(argumento, str)):
        raise ValueError (funcao+":argumento invalido")
    return

def max_i(string):
    """cadeia de caracteres -> inteiro
        Devolve o valor maximo para um indice para string ser uma silaba"""
    if(len(string) < 5):
        return len(string)
    else:
        return 5

def min_i(string):
    """cadeia de caracteres -> inteiro
        Devolve o valor minimo para um indice para string ser uma silaba"""
    if(len(string)-5 < 0):
        return 0
    else:
        return len(string)-5

def e_palavra(string):
    """cadeia de caracteres -> booleano
        Verifica se string e uma palavra, de acordo com a gramatica"""

    def silaba_recur(string):   # cadeia de caracteres -> booleano
                                # Verifica recursivamente se uma string e uma palavra (sem silaba final)
        if(string == ""):
            return True
        for i in range(5, -1, -1):  # Verificar se e silaba, da maior para a menor
            if(e_silaba(string[:i]) and silaba_recur(string[i:])):
                return True
        return False

    verifica_arg(string, "e_palavra")
    if(e_monossilabo(string)):
        return True

    for i in range(min_i(string), len(string)): # Verificar a existencia de uma silaba final
        if(e_silaba_final(string[i:]) and silaba_recur(string[:i])):
            return True
    return False
