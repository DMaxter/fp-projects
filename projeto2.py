# Aluno Daniel Maximiano Matos    Numero 89429

from parte1 import e_palavra
from itertools import permutations

# TAD palavra_potencial

def cria_palavra_potencial(string, tuplo):
    """cria_palavra_potencial: string x tuplo -> palavra_potencial
    Construtor do tipo palavra_potencial
    Verifica se a string e formada so por elementos do tuplo
    Recebe uma string e um tuplo de letras
    Devolve um objeto do tipo palavra_potencial"""
    if(isinstance(string, str) and isinstance(tuplo, tuple)):
        lista = []
        # Verifica a validade do tuplo e copia-o para uma lista
        for e in tuplo:
            if(not("A" <= e <= "Z")):
                raise ValueError ("cria_palavra_potencial:argumentos invalidos.")
            else:
                lista += [e]
        # Verifica a validade da string
        for e in string:
            if(not("A" <= e <= "Z")):
                raise ValueError ("cria_palavra_potencial:argumentos invalidos.")
            elif(e not in lista):
                raise ValueError ("cria_palavra_potencial:a palavra nao e valida.")
            else:
            # Se a letra da string estiver na lista, remove-se esse elemento da lista
                lista.remove(e)

        return string
    else:
        raise ValueError ("cria_palavra_potencial:argumentos invalidos.")

def palavra_tamanho(palavra):
    """palavra_tamanho: palavra_potencial -> inteiro
    Seletor do tipo palavra_potencial
    Recebe um elemento do tipo palavra_potencial
    Devolve o tamanho da palavra"""
    if(e_palavra_potencial(palavra)):
        return len(palavra)

def e_palavra_potencial(arg):
    """e_palavra_potencial: universal -> logico
    Reconhecedor do tipo palavra_potencial
    Verifica se arg e do tipo palavra_potencial
    Recebe um elemento de qualquer tipo
    Devolve \"True\" ou \"False\""""
    if(isinstance(arg, str)):
        for e in arg:
            if(not("A" <= e <= "Z")):
                return False
        return True
    else:
        return False

def palavras_potenciais_iguais(palavra1, palavra2):
    """palavras_potenciais_iguais: palavra_potencial x palavra_potencial -> logico
    Teste para o tipo palavra_potencial
    Verifica se palavra1 e palavra2 sao iguais
    Recebe duas palavras potenciais
    Devolve \"True\" ou \"False\""""
    return (e_palavra_potencial(palavra1) and e_palavra_potencial(palavra2)\
            and (palavra1 == palavra2))

def palavra_potencial_menor(palavra1, palavra2):
    """palavra_potencial_menor: palavra_potencial x palavra_potencial -> logico
    Teste para o tipo palavra_potencial
    Verifica se palavra1 e menor que palavra2
    Recebe dois elementos do tipo palavra_potencial
    Devolve \"True\" ou \"False\""""
    if(e_palavra_potencial(palavra1) and e_palavra_potencial(palavra2)):
        return palavra1 < palavra2

def palavra_potencial_para_cadeia(palavra):
    """palavra_potencial_para_cadeia: palavra_potencial -> string
    Transformador de saida para o tipo palavra_potencial
    Recebe um elemento do tipo palavra_potencial
    Devolve a palavra_potencial introduzida convertida para string"""
    if(e_palavra_potencial(palavra)):
        return palavra

# TAD conjunto_palavras

def cria_conjunto_palavras():
    """cria_conjunto_palavras: -> conjunto_palavras
    Construtor do tipo conjunto_palavras
    Devolve um objeto do tipo conjunto_palavras, vazio"""
    return {}

def numero_palavras(conjunto):
    """numero_palavras: conjunto_palavras -> inteiro
    Seletor do tipo conjunto_palavras
    Recebe um elemento do tipo conjunto_palavras
    Devolve o numero de palavras no conjunto"""
    if(e_conjunto_palavras(conjunto)):
        num = 0
        for i in conjunto:
            num += len(conjunto[i])

        return num

def subconjunto_por_tamanho(conjunto, numero):
    """subconjunto_por_tamanho: conjunto_palavras_para_cadeia x inteiro -> lista
    Seletor do tipo conjunto_palavras
    Recebe um elemento do tipo conjunto_palavras
    Devolve as palavras do conjunto com tamanho igual a numero"""
    if(e_conjunto_palavras(conjunto) and isinstance(numero, int)):
        if(numero in list(conjunto)):
            return conjunto[numero]
        else:
            return []

def acrescenta_palavra(conjunto, palavra):
    """acrescenta_palavra: conjunto_palavras x palavra_potencial ->
    Modificador do tipo conjunto_palavras
    Recebe um elemento do tipo conjunto_palavras e um elemento do tipo palavra_potencial
    Adiciona a palavra ao conjunto"""
    if(e_conjunto_palavras(conjunto) and e_palavra_potencial(palavra)):
        tamanho = palavra_tamanho(palavra)
        if(tamanho in list(conjunto)):
            if(palavra not in conjunto[tamanho]):
                conjunto[tamanho] += [palavra]
                conjunto[tamanho].sort()
        else:
            conjunto[tamanho] = [palavra]
    else:
        raise ValueError ("acrescenta_palavra:argumentos invalidos.")

def e_conjunto_palavras(arg):
    """e_conjunto_palavras: universal -> logico
    Reconhecedor do tipo conjunto_palavras
    Verifica se arg e do tipo conjunto_palavras
    Recebe um elemento de qualquer tipo
    Devolve \"True\" ou \"False\""""
    if(isinstance(arg, dict)):
        for i in arg:
            if(not(isinstance(i, int) and isinstance(arg[i], list))):
                return False
            for j in arg[i]:
                if(not(e_palavra_potencial(j))):
                    return False
        return True
    else:
        return False

def conjuntos_palavras_iguais(conjunto1, conjunto2):
    """conjuntos_palavras_iguais: conjunto_palavras x conjunto_palavras -> logico
    Teste do tipo conjunto_palavras
    Verifica se dois conjuntos de palavras sao iguais
    Recebe dois elementos do tipo conjunto_palavras
    Devolve \"True\" ou \"False\""""
    return (e_conjunto_palavras(conjunto1) and e_conjunto_palavras(conjunto2)\
            and (numero_palavras(conjunto1) == numero_palavras(conjunto2))\
            and conjunto1 == conjunto2)

def conjunto_palavras_para_cadeia(conjunto):
    """conjunto_palavras_para_cadeia: conjunto_palavras -> string
    Transformador de saida para o tipo conjunto_palavras
    Recebe um elemento do tipo conjunto_palavras
    Devolve uma string com todos os elementos do conjunto, agrupados por tamanho, por ordem crescente"""
    if(e_conjunto_palavras(conjunto)):
        string = ""
        lista = list(conjunto)
        lista.sort()
        # Coloca os elementos do conjunto numa string
        for i in lista:
            lista1 = subconjunto_por_tamanho(conjunto, i)
            string += str(i)+"->"+str(lista1)+";"
        # Remove-se o ultimo caracter ';' de string
        string = "[" + string[:-1] + "]"
        return string.replace("'", "")

# TAD Jogador

def cria_jogador(nome):
    """cria_jogador: string -> jogador
    Construtor do tipo jogador
    Recebe o nome do jogador
    Devolve um objeto do tipo jogador"""
    if(isinstance(nome, str)):
        validas = cria_conjunto_palavras()
        invalidas = cria_conjunto_palavras()
        return {"nome":nome, "pontos":0, "p_validas":validas,\
                "p_invalidas":invalidas}
    else:
        raise ValueError ("cria_jogador:argumento invalido.")

def jogador_nome(jogador):
    """jogador_nome: jogador -> string
    Seletor do tipo jogador
    Recebe um jogador
    Devolve o nome do jogador"""
    if(e_jogador(jogador)):
        return jogador["nome"]

def jogador_pontuacao(jogador):
    """jogador_pontuacao: jogador -> inteiro
    Seletor do tipo jogador
    Recebe um jogador
    Devolve a pontuacao do jogador"""
    if(e_jogador(jogador)):
        return jogador["pontos"]

def jogador_palavras_validas(jogador):
    """jogador_palavras_validas: jogador -> conjunto_palavras
    Seletor do tipo jogador
    Recebe um jogador
    Devolve o conjunto de palavras validas introduzidas pelo jogador"""
    if(e_jogador(jogador)):
        return jogador["p_validas"]

def jogador_palavras_invalidas(jogador):
    """jogador_palavras_invalidas: jogador -> conjunto_palavras
    Seletor do tipo jogador
    Recebe um jogador
    Devolve o conjunto de palavras invalidas introduzidas pelo jogador"""
    if(e_jogador(jogador)):
        return jogador["p_invalidas"]

def adiciona_palavra_valida(jogador, palavra):
    """adiciona_palavra_valida: jogador x palavra_potencial ->
    Modificador do tipo jogador
    Recebe um jogador e uma palavra_potencial para adicionar ao conjunto de palavras validas introduzidas por este
    Adiciona a palavra ao conjunto de palavras validas introduzidas pelo jogador"""
    if(e_jogador(jogador) and e_palavra_potencial(palavra)):
        # Verifica se houve alteracoes, para atribuir (ou nao) a pontuacao
        conjunto = jogador_palavras_validas(jogador)
        antes = numero_palavras(conjunto)
        acrescenta_palavra(conjunto, palavra)
        depois = numero_palavras(conjunto)
        # Se os conjuntos de palavras tiverem igual tamanho antes e depois, nao se incrementa a pontucao
        if(not(antes == depois)):
            jogador["pontos"] += palavra_tamanho(palavra)
    else:
        raise ValueError ("adiciona_palavra_valida:argumentos invalidos.")

def adiciona_palavra_invalida(jogador, palavra):
    """adiciona_palavra_valida: jogador x palavra_potencial ->
    Modificador do tipo jogador
    Recebe um jogador e uma palavra_potencial para adicionar ao conjunto de palavras invalidas introduzidas por este
    Adiciona a palavra ao conjunto de palavras invalidas introduzidas pelo jogador"""
    if(e_jogador(jogador) and e_palavra_potencial(palavra)):
        # Verifica se houve alteracoes, para atribuir (ou nao) a pontuacao
        conjunto = jogador_palavras_invalidas(jogador)
        antes = numero_palavras(conjunto)
        acrescenta_palavra(conjunto, palavra)
        depois = numero_palavras(conjunto)
        # Se os conjuntos de palavras tiverem igual tamanho antes e depois, nao se decrementa a pontucao
        if(not(antes == depois)):
            jogador["pontos"] -= palavra_tamanho(palavra)
    else:
        raise ValueError ("adiciona_palavra_invalida:argumentos invalidos.")

def e_jogador(arg):
    """e_jogador: universal -> logico
    Reconhecedor do tipo jogador
    Verifica se arg e do tipo jogador
    Recebe um elemento de qualquer tipo
    Devolve \"True\" ou \"False\""""
    return (isinstance(arg, dict) and len(arg) == 4\
            and ("nome" in arg)\
            and ("pontos" in arg)\
            and ("p_validas" in arg)\
            and ("p_invalidas" in arg)
            and isinstance(arg["nome"], str)\
            and isinstance(arg["pontos"], int)\
            and e_conjunto_palavras(arg["p_validas"])\
            and e_conjunto_palavras(arg["p_invalidas"]))

def jogador_para_cadeia(jogador):
    """jogador_para_cadeia: jogador -> string
    Transformador de saida para o tipo jogador
    Recebe um jogador
    Devolve uma string com todos os dados relativos ao jogador"""
    if(e_jogador(jogador)):
        return ("JOGADOR "+jogador["nome"]+\
                " PONTOS="+str(jogador_pontuacao(jogador))+\
                " VALIDAS="+conjunto_palavras_para_cadeia(jogador["p_validas"])+\
                " INVALIDAS="+conjunto_palavras_para_cadeia(jogador["p_invalidas"]))

def gera_todas_palavras_validas(tuplo):
    """gera_todas_palavras_validas: tuplo -> conjunto_palavras
    Gera todas as palavras validas, segundo a gramatica, com as letras do tuplo
    Recebe um tuplo com letras
    Devolve um elemento do tipo conjunto_palavras com todas as palavras validas, usando as letras do tuplo"""
    lista = []
    for i in range(1,len(tuplo)+1):
        lista += list(permutations(tuplo, i))

    conjunto = cria_conjunto_palavras()
    # Verifica, de todas, quais sao as palavras validas segundo a gramatica e coloca-as num elemento do tipo conjunto_palavras
    for e in lista:
        string = ""
        for j in e:
            string += j
        if(e_palavra(string)):
            palavra = cria_palavra_potencial(string, tuplo)
            acrescenta_palavra(conjunto, palavra)

    return conjunto

def guru_mj(tuplo):
    """guru_mj: tuplo ->
    Inicia o modo multijogador do jogo \"Palavra Guru\"
    Recebe um tuplo com as letras que vao ser jogadas"""
    print("Descubra todas as palavras geradas a partir das letras:")
    print(str(tuplo))

    jogadores = jogar(recebe_jogadores(), tuplo)

    string = "FIM DE JOGO! "
    indice, resultado = verifica_resultado(jogadores)

    if(resultado == "V"):
        string += "O jogo terminou com a vitoria do jogador "+\
                    jogador_nome(jogadores[indice])+\
                    " com "+str(jogador_pontuacao(jogadores[indice]))+\
                    " pontos."
    elif(resultado == "E"):
        string += "O jogo terminou em empate."

    print(string)

    for i in jogadores:
        print(jogador_para_cadeia(i))

def recebe_jogadores():
    """recebe_jogadores: -> lista
    Cria uma lista com os jogadores criados pelo(s) utilizador(es)
    Devolve um lista com os jogadores introduzidos"""
    jogadores = []

    print("Introduza o nome dos jogadores (-1 para terminar)...")
    n = 0
    nome = input("JOGADOR "+str(n+1)+" -> ")

    while(nome != "-1"):
        jogadores = jogadores + [cria_jogador(nome)]
        n = n + 1
        nome = input("JOGADOR "+str(n+1)+" -> ")

    return jogadores

def jogar(jogadores, tuplo):
    """jogar: lista x tuplo -> lista
    Ambiente de jogo
    Recebe a lista com todos os jogadores e o tuplo com as letras em jogo
    Devolve a lista com todos os jogadores"""
    todas_p_validas =  gera_todas_palavras_validas(tuplo)
    p_certas = cria_conjunto_palavras()
    n_jogadores = len(jogadores)
    k = 1
    n = 0

    while(not(conjuntos_palavras_iguais(p_certas, todas_p_validas)\
            or len(jogadores) == 0)):
        print("JOGADA "+str(k)+" - Falta descobrir "+\
                str(numero_palavras(todas_p_validas)-numero_palavras(p_certas))+\
                " palavras")

        tentativa = cria_palavra_potencial(input("JOGADOR "+jogador_nome(jogadores[n])+" -> "), tuplo)

        # Verifica se a tentativa esta no conjunto das palavras validas
        if(tentativa in subconjunto_por_tamanho(todas_p_validas, palavra_tamanho(tentativa))):
            if(tentativa not in subconjunto_por_tamanho(p_certas, palavra_tamanho(tentativa))):
                adiciona_palavra_valida(jogadores[n], tentativa)
                acrescenta_palavra(p_certas, tentativa)
            print(palavra_potencial_para_cadeia(tentativa)+" - palavra VALIDA")
        else:
            tentativa = cria_palavra_potencial(tentativa, tuplo)
            adiciona_palavra_invalida(jogadores[n], tentativa)
            print(palavra_potencial_para_cadeia(tentativa)+" - palavra INVALIDA")

        k = k + 1
        n = n + 1
        # Se tivermos chegado ao final da lista, reiniciamos a iteracao
        if(n == n_jogadores):
            n = 0

    return jogadores

def verifica_resultado(jogadores):
    """verifica_resultado: lista -> tuplo
    Verifica o resultado do jogo comparando as pontuacoes
    Recebe a lista com os jogadores
    Devolve um tuplo com o indice do primeiro jogador vencedor e o resultado (\"V\" - vitoria ou \"E\" - empate)"""
    pont_max = jogador_pontuacao(jogadores[0])
    indice = 0
    resultado = "V"

    for i in range(1, len(jogadores)):
        pontos = jogador_pontuacao(jogadores[i])

        if(pont_max < pontos):
            pont_max = pontos
            indice = i
            resultado = "V"
        elif(pontos == pont_max):
            resultado = "E"

    return (indice, resultado)
